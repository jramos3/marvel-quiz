const STORE = [
    {
      questionNumber: 1,
      question: "What is the name of Thor’s hammer?",
      answer1: 'The Chosen One',
      answer2: 'Mjolnir',
      answer3: 'Almighty Hammer',
      answer4: 'Thunder Pound',
      correctAnswer: 'Mjolnir'
    },
    {
      questionNumber: 2,
      question: "What is Captain America’s shield made out of?",
      answer1: 'Gold',
      answer2: 'Copper',
      answer3: 'Steel',
      answer4: 'Vibranium',
      correctAnswer: 'Vibranium'
    },
    {
      questionNumber: 3,
      question: "Who can lift Thor’s hammer?",
      answer1: 'Just Thor',
      answer2: 'Captain America',
      answer3: 'Peter Parker',
      answer4: 'Loki',
      correctAnswer: 'Captain America'
    },
    {
      questionNumber: 4,
      question:
        'Who is Black Panther’s sister?',
      answer1: 'Shuri',
      answer2: 'Natasha',
      answer3: 'Gamora',
      answer4: 'Wanda',
      correctAnswer: 'Shuri'
    },
    {
      questionNumber: 5,
      question:
        "What is Hulk’s secret?",
      answer1: 'He hates Tony',
      answer2: 'He prefers being Hulk',
      answer3: 'He’s always angry',
      answer4: "He wants to take over the avengers",
      correctAnswer: 'He’s always angry'
    }
  ];
  