function handleStartQuiz() {
    $('.nextQuestion').on('click', function() {
      $('header').hide();
      $('form').show();
      $('#container').show();
      $('footer').show();
      $('.questionAnswerForm').hide();
      console.log('program is working');
    });
  }
  
  let questionNumber = 1;
  let score = 0;
  
  function generateQuestion() {
    iterateQuestion();
    $('.results').hide();
    return `
  
    <section class="questionPage" role="Question Page">
      <h2 class="question">${STORE[questionNumber - 1].question}</h2>
        <form role="bsr-form" accept-charset="UTF-8">
          <fieldset>
          <div class="form-group">
            <div class="radio">
            <label>
              <input type="radio" class="answer" name="option" value="${
                STORE[questionNumber - 1].answer1
              }"/>
              ${STORE[questionNumber - 1].answer1}<br>
            </label>
            </div>
            <div class="radio">
            <label>
              <input type="radio" class="answer" name="option" value="${
                STORE[questionNumber - 1].answer2
              }"/>
              ${STORE[questionNumber - 1].answer2}<br>
            </label>
            </div>
            <div class="radio">
            <label>
              <input type="radio" class="answer" name="option" value="${
                STORE[questionNumber - 1].answer3
              }"/>
              ${STORE[questionNumber - 1].answer3}<br>
            </label>
            </div>
            <div class="radio">
            <label>
              <input type="radio" class="answer" name="option" value="${
                STORE[questionNumber - 1].answer4
              }"/>
              ${STORE[questionNumber - 1].answer4}<br>
            </label>
            </div>
            </div>
          </fieldset>
        </form>
        <button type="submit" class="getResults btn btn-primary">Submit</button>
        </section>
  
      `;
    console.log('generateQuestion is working');
  }
  
  function renderQuestion() {
    console.log('renderQuestion question number', questionNumber);
    $('#container').html(generateQuestion());
    $('header').hide();
    $('form').show();
    $('#container').show();
    $('.questionAnswerForm').hide();
  }
  
  function handleNextQuestion() {
    $('.nextQuestion').on('click', function() {
      if (questionNumber <= STORE.length) {
        renderQuestion();
        questionNumber++;
      } else {
        renderResults();
      }
      console.log('handleNextQuestion is working');
    });
  }
  
  function handleAnswerSubmitted() {
    $('#container').on('click', '.getResults', function() {
      checkUserAnswer();
      $('.questionAnswerForm').show();
    });
  }
  
  function checkUserAnswer() {
    event.preventDefault();
    //Need to fix this so there's no -2 value
    let correctAnswer = `${STORE[questionNumber - 2].correctAnswer}`;
    let userAnswer = $('input:checked').val();
  
    if (userAnswer == correctAnswer) {
      console.log('checkUserAnswer score', score);
      correctAnswerFeedback();
    } else {
      console.log('incorrectAnswer selected');
      incorrectAnswerFeedback();
    }
  }
  
  function correctAnswerFeedback() {
    score++;
    $('header').hide();
    $('form').hide();
    $('#container').hide();
    $('footer').show();
    $('.questionAnswerForm').html(`
      <div role="banner" class="results">
      <h2>Correct!</h2>
        <iframe src="https://media.giphy.com/media/kkYbDLFmNvO4E/giphy.gif" width="200" height="230" frameBorder="0" class="giphy" allowFullScreen></iframe>
          <button type="submit" class="nextQuestion btn btn-default">Next Question</button>
      </div>`);
    if (questionNumber <= STORE.length) {
      handleNextQuestion();
    } else {
      renderResults();
    }
  }
  
  function incorrectAnswerFeedback() {
    $('header').hide();
    $('form').hide();
    $('#container').hide();
    $('footer').show();
    $('.questionAnswerForm').html(`<div role="banner" class="results">
      <h2>Wrong! The Correct Answer Is ${
        STORE[questionNumber - 2].correctAnswer
      }!</h2>
      <iframe src="https://media.giphy.com/media/ycBW5N5XMfpXTvamtF/giphy.gif" width="480" height="370" frameBorder="0" class="giphy" allowFullScreen></iframe>
      <button type="submit" class="nextQuestion btn btn-default">Next Question</button>
      </div>`);
    if (questionNumber <= STORE.length) {
      handleNextQuestion();
    } else {
      renderResults();
    }
  }
  
  function iterateQuestion() {
    $('.currentQuestion').html(`<p>Question: ${questionNumber} / 5</p>`);
  }
  // do this again
  function generateResults() {
  
    return $('.resultsPage')
      .html(`
      <div class="container">
        <header role="banner"><h1 class="resultsHeader">Thanks for playing!</h1>
          <p><iframe src="https://media.giphy.com/media/ULrXjdLIA5M5O/giphy.gif" width="480" height="350" frameBorder="0" class="giphy" alt="Voldemort Avada Kedavra" allowFullScreen></iframe></p>
          <h2>You Got ${score} Out Of 5 Correct</h2>
          <button type="submit" class="restartQuiz btn btn-primary">Restart Quiz</button>
        </header >
      </div>`);
    
  }
  
  function renderResults() {
    $('.nextQuestion').on('click', function() {
      $('.results').hide();
      generateResults();
      console.log('renderResults is working');
      restartQuiz();
    });
  }
  
  function restartQuiz() {
    $('.restartQuiz').on('click', function() {
      location.reload();
      console.log('restartQuiz is working');
    });
  }
  
  function handleQuiz() {
    handleStartQuiz();
    handleAnswerSubmitted();
    handleNextQuestion();
    restartQuiz();
  }
  
  $(handleQuiz);
  